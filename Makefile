omegascan:
	for INI in `ls */*ini` ; do OUT=$$(echo $${INI} | sed -e s/.ini/_omegascan.txt/) ; echo "converting $${INI} to $${OUT}" ; tools/clf-to-omegascan.py -f -o $${OUT} $${INI} ; done

kleinewelle:
	for DIR in `ls O*/ ER*/ -d` ; do for INI in `ls $${DIR}/*ini` ; do echo "converting $${INI} to KW configs" ; tools/clf-to-kleinewelle.py -f -o $${DIR} $${INI} -V ; done ; done
