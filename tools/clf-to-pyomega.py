#!/usr/bin/env python

"""Convert a LIGO Channel List INI file into an Omega channels.list file
"""

from __future__ import (print_function, division)

import argparse
import re
import fnmatch
from collections import OrderedDict
from math import (pi, ceil, log)

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser

from numpy import (inf, isinf)

from gwpy.detector import ChannelList

# global variables
NAMES = {
    'GW': 'Gravitational Wave Strain',
    'GDS': 'Alternate Strain Versions',
    'CAL': 'Calibrated h(t)',
    'LSC': 'Length Sensing and Control',
    'ASC': 'Alignment Sensing and Control',
    'ALS': 'Arm Length Stabilization',
    'HPI': 'Hydraulic External Pre-Isolator',
    'IMC': 'Input Mode Cleaner',
    'OMC': 'Output Mode Cleaner',
    'PSL': 'Pre-Stabilized Laser',
    'ISI': 'Internal Seismic Isolation',
    'SUS': 'Suspensions',
    'PEM': 'Physical Environment Monitoring',
    'TCS': 'Thermal Compensation System',
    'SQZ': 'Optical Squeezer',
    'NGN': 'Newtonian Noise Array',
    'ADS': 'Alignment Dither System',
    'AOS': 'Auxiliary Optics',
    'LAS': 'Laser',
    'MZM': 'Mach-Zehnder Modulator',
    'VIS': 'Vibration Isolation System',
}

LOCK_STATES = {
    'G1': 'G1:GEO-UP:1',
    'V1': 'V1:ITF_NOMINAL_LOCK:1',
    'K1': None,
    'H1': 'H1:DMT-GRD_ISC_LOCK_NOMINAL:1',
    'L1': 'L1:DMT-GRD_ISC_LOCK_NOMINAL:1',
}


def dsp_parameters(flow, qhigh, pem=False):
    """Calculate segment parameters for Omega
    """
    t0 = 64 if pem else 32
    # get input duration
    duration = int(max(t0, 2 ** ceil(log(qhigh / (2 * pi * flow), 2))))
    # fftlength is set to resolve important lines
    fftlength = int(max(8, 2 ** ceil(log(1./flow, 2))))
    fftlength = min(fftlength, duration/2)
    return (duration, fftlength)


def get_subsystem(channel):
    """Given a channel, return the subsystem it belongs to
    """
    name = channel.name.split(':')[1]
    return name.split('-')[0]


parser = argparse.ArgumentParser()
parser.add_argument('clf', help='path of channel list file')
parser.add_argument('-o', '--output-file', default='omega-channels.ini',
                    help='path of output Omega channels file')
parser.add_argument('--pem', action='append',
                    default=['*PEM-*', '*ISI-GND_*', '*NGN-*'],
                    help='list of glob-style regex patterns to match channels '
                         'that should run all the time, default: %(default)s')
parser.add_argument('--primary', default=None,
                    help='primary channel for cross-correlation, default: '
                         '%(default)s')
parser.add_argument('--priority', action='append',
                    default=['G1:DER_DATA_H', '[HL]1:GDS-CALIB_STRAIN_CLEAN',
                             'V1:Hrec_hoft_[0-9]*[0-9]Hz'],
                    help='list of glob-style regex patterns to match priority '
                         'channels, default: %(default)s')
parser.add_argument('--use-dmt-hoft', action='store_true', default=False,
                    help='switch to using DMT_C00 frame files for h(t)')
parser.add_argument('--plot-duration', action='append',
                    default=['1', '4', '16'],
                    help='time axis durations on Omega scan plots, may be '
                         'specified multiple times, defaults to [1, 4, 16] '
                         'seconds')
parser.add_argument('--allow-unsafe-channels', action='store_true',
                    default=False,
                    help='allow unsafe channels to be analyzed, default: '
                         '%(default)s')
parser.add_argument('--allow-glitchy-channels', action='store_true',
                    default=False,
                    help='allow glitchy channels to be analyzed, default: '
                         '%(default)s')
parser.add_argument('--nyquist', action='store_true', default=False,
                    help='extend all channels up to their Nyquist frequency, '
                         'default: %(default)s')
parser.add_argument('--low-frequency', type=int,
                    help='sets low frequency bound of all channels to '
                    'this value')
parser.add_argument('--sample-rate-cut', type=float,
                    help='only channels with sample rate above this '
                    'value will be added to the configuration file, channels '
                    'with specified high frequency bounds will be removed')

args = parser.parse_args()

re_priority = re.compile('(%s)' % '|'.join(
    map(fnmatch.translate, args.priority)))
re_pem = re.compile('(%s)' % ('|'.join(
    map(fnmatch.translate, args.pem))))

# set plotting defaults
plot_duration = args.plot_duration

# read channel list file
channels = ChannelList.read(args.clf)

# build groups
groups = OrderedDict()
for channel in channels:
    # remove 'flat' channels
    fidelity = channel.params.get('fidelity', 'clean').lower()
    if fidelity == 'flat':
        continue
    # remove 'glitchy' channels
    if (fidelity == 'glitchy') and not args.allow_glitchy_channels:
        continue
    # remove 'unsafe' and 'unsafeabove2kHz' channels, except for h(t)
    if not args.allow_unsafe_channels:
        safety = channel.params.get('safe', 'unknown').lower()
        if ('unsafe' in safety) and ('CALIB_STRAIN' not in channel.name):
            continue

    # add channel to group list
    sf = channel.sample_rate.value
    fr = tuple(channel.frequency_range.value)
    # if a sample rate cut is provided, remove all channels below threshold
    # and any channels with a pre-existing fhigh
    if args.sample_rate_cut and (sf < args.sample_rate_cut or
            not isinf(fr[1])):
        continue
    try:
        qh = float(channel.params['qhigh'])
    except KeyError:
        qh = 64.
    finally:
        qr = (3.3166, float(qh))
    if isinf(fr[1]):
        flow = args.low_frequency if args.low_frequency else fr[0]
        fhigh = (
            int(sf / 2) if args.nyquist else
            round(channel.sample_rate.value / 2 / (1 + 11**(1/2.) / qr[0]))
        )
        fr = (flow, fhigh)
    if re_pem.search(channel.name):
        state = None
    else:
        state = LOCK_STATES[channel.ifo]
    priority = (re_priority.search(channel.name) is not None)
    frametype = channel.frametype or '{ifo}_R'.format(ifo=channel.ifo)
    if (args.use_dmt_hoft and
            frametype == '{ifo}_HOFT_C00'.format(ifo=channel.ifo)):
        frametype = '{ifo}_DMT_C00'.format(ifo=channel.ifo)
    subsyst = get_subsystem(channel)
    key = (subsyst, sf, frametype, fr, qr, state, priority)
    try:
        groups[key].append(channel.name)
    except KeyError:
        groups[key] = [channel.name]

    # determine primary channel
    if channel.name == args.primary:
        pem = (get_subsystem(channel) == 'PEM')
        duration, fftlength = dsp_parameters(fr[0], qh, pem=pem)
        primary_args = OrderedDict([
            ('f-low', str(fr[0])),
            ('resample', '4096' if (sf > 4096 and not args.nyquist) else '0'),
            ('frametype', frametype),
            ('duration', str(duration)),
            ('fftlength', str(fftlength)),
            ('matched-filter-length', '6'),
            ('channel', channel.name),
        ])

# write omega channels.list file
outclf = ConfigParser(dict_type=OrderedDict)

# start with primary channel
try:
    outclf.add_section('primary')
    for key, val in primary_args.items():
        outclf.set('primary', key, val)
    print('primary')
except Exception:
    pass

# move on to the full list
count = 0
for i, key in enumerate(groups):
    subsyst, sf, ft, fr, qr, state, priority = key
    if priority:
        name = 'GW'
        always_plot = True
    else:
        name = subsyst
        always_plot = False
    if not outclf.has_section(name):
        parent = None
        outclf.add_section(name)
        outclf.set(name, 'name', NAMES[name])
    else:
        count += 1
        parent = name
        name = '-'.join([name, str(int(sf)), str(count)])
        outclf.add_section(name)
    if qr is not None:
        outclf.set(name, 'q-range', ','.join(map(str, qr)))
    outclf.set(name, 'frequency-range', ','.join(map(str, fr)))
    # if faster than 4096 Hz, resample
    if sf > 4096 and not args.nyquist:
        outclf.set(name, 'resample', str(4096))
    outclf.set(name, 'frametype', ft)
    if state:
        outclf.set(name, 'state-flag', state)
    duration, fftlength = dsp_parameters(fr[0], qh, pem=(subsyst == 'PEM'))
    outclf.set(name, 'duration', str(duration))
    outclf.set(name, 'fftlength', str(fftlength))
    if name == 'GW':
        outclf.set(name, 'max-mismatch', '0.2')
        outclf.set(name, 'snr-threshold', '5')
    else:
        outclf.set(name, 'max-mismatch', '0.35')
        outclf.set(name, 'snr-threshold', '5.5')
    if parent:
        outclf.set(name, 'parent', parent)
    outclf.set(name, 'always-plot', str(always_plot))
    outclf.set(name, 'plot-time-durations', ','.join(plot_duration))
    outclf.set(name, 'channels', '\n'.join(groups[key]))
    print(name)

with open(args.output_file, 'w') as f:
    outclf.write(f)
